#!/usr/bin/python
"""
http://wammu.eu/docs/manual/formats/smsbackup.html
http://android.riteshsahu.com/apps/sms-backup-restore

Author: Keyvan Hedayati <k1.hedayati93@gmail.com>
License GNU General Public License, version 3
"""

import configparser
import math
import xml.dom.minidom
import datetime
import sys

def decodeText(encodedText,length):
    decodedText = ""
    # This is complicated code and I could not find any way simpler.
    # Data is stored in unicode hex, so each 4 hex value is a character
    # I fetch each 4 byte if hex code then convert it to unicode character
    # and at last add it to current message.
    for pos in range(length):
        decodedText += chr(int(encodedText[pos*4:pos*4+4],base=16))
    return decodedText

def readOldMsgFile(filename):
    config = configparser.ConfigParser()
    config.read(filename, "iso8859_15") # encoding of backup file is iso8859_15
    sections = config.sections()
    messages=[]
    for key in sections:
        encodedText = ""
        sms={}
        # Get message info from file
        sms['SMSC'] = config[key]['SMSC'][1:-1] if(config.has_option(config[key].name,'SMSC')) else "null" # Text representation of SMSC number. [1:-1] removes "" from first and end of number
        sms['number'] = config[key]['Number'][1:-1] if(config.has_option(config[key].name,'Number')) else "null" # Recipient number.
        sms['time'] = config[key]['DateTime'] if config.has_option(config[key].name,'DateTime') else "null" # Timestamp of message (sent or received).
        sms['folder'] = config[key]['Folder'] if config.has_option(config[key].name,'Folder') else "null" # ID of folder where the message was saved.
        sms['state'] = config[key]['State'] if config.has_option(config[key].name,'State') else "null" # State of the message: Read, UnRead, Sent, UnSent
        sms['PDU'] = config[key]['PDU'] if config.has_option(config[key].name,'PDU') else "null" # Message type: Deliver - received message, Submit - message to send, Status_Report - message to send with delivery report
        sms['length'] = int(config[key]['Length']) if config.has_option(config[key].name,'Length') else 0 # Length of message text.
        
        # Covert time to a standard datetime object
        if sms['time']!="null":
            sms['time'] = datetime.datetime.strptime(sms['time'],'%Y%m%dT%H%M%SZ')
        
        if config.has_option(config[key].name,'Text00'):
            # Message text is spilted into 50 characters.
            # Text00 ... TextNN: Numbered parts of the message payload.
            for textNum in range(math.ceil(sms['length'] / 50)): 
                textId = "Text%02d" % (textNum)
                encodedText += config[key][textId]
        
        sms["msg"] = decodeText(encodedText,sms['length'])
        messages.append(sms)
    return messages

def makeXml(messages):
    # Fields in the XML File
    #   protocol – Protocol used by the message, its mostly 0 in case of SMS messages.
    #   address – The phone number of the sender/recipient.
    #   date – The Java date representation (including millisecond) of the time when the message was sent/received. Check out www.epochconverter.com for information on how to do the conversion from other languages to Java.
    #   type – 1 = Received, 2 = Sent, 3 = Draft, 4 = Outbox, 5 = Failed, 6 = Queued
    #   subject – Subject of the message, its always null in case of SMS messages.
    #   body – The content of the message.
    #   toa – n/a, default to null.
    #   sc_toa – n/a, default to null.
    #   service_center – The service center for the received message, null in case of sent messages.
    #   read – Read Message = 1, Unread Message = 0.
    #   status – None = -1, Complete = 0, Pending = 32, Failed = 64.
    #   readable_date – Optional field that has the date in a human readable format.
    #   contact_name – Optional field that has the name of the contact.
    smses = xml.dom.minidom.Document.createElement(None,'smses')
    smses.setAttribute("count",str(len(messages)))
    for message in messages:
        sms = xml.dom.minidom.Document.createElement(None,'sms')
        sms.setAttribute("protocol","0")
        sms.setAttribute("address",message['number'])
        if message['time'] != "null":
            sms.setAttribute("date",str(int(message['time'].timestamp())*1000)) # Convert date to "The Java date representation"
        else:
            sms.setAttribute("date","null")
        
        if message['PDU']=="Deliver":
            sms.setAttribute("type","1")
        elif message['PDU']=="Submit":
            sms.setAttribute("type","2")
        
        sms.setAttribute("subject","null")
        sms.setAttribute("body",message['msg'])
        sms.setAttribute("toa","null")
        sms.setAttribute("sc_toa","null")
        sms.setAttribute("service_center",message['SMSC'])
        if message['state']=="Read":
            sms.setAttribute("read","1")
        elif message['state']=="UnRead":
            sms.setAttribute("read","0")
        
        sms.setAttribute("status","0")
        if message['time'] != "null":
            sms.setAttribute("readable_date",message['time'].strftime("%b %d, %Y %I:%M:%S %p"))
        else:
            sms.setAttribute("readable_date","null")
        
        sms.setAttribute("contact_name","(Unknown)")
        smses.appendChild(sms)
    return smses
    
def writeXml(xml, outputFile):
        f = open(outputFile, "w")
        f.write("<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>\n")
        f.write(xml.toprettyxml())
        f.close()

if __name__ == '__main__':
    # check for cmmand line arguments
    if len(sys.argv) < 3:
        print("Usage: python %s oldsmsfile.smsbackup xmlsmsfile.xml" % sys.argv[0])
    else:
        oldsmsfile = sys.argv[1]
        xmlsmsfile = sys.argv[2]
        messages = readOldMsgFile(oldsmsfile)
        xml = makeXml(messages)
        writeXml(xml,xmlsmsfile)