###Description
This script converts wammu(gammu) sms backup file to xml file, specifically to android SMS Backup & Restore xml.
So you can import your old messages to your new android device.

###Specifications
+ Uses configparser and xml.dom.minidom modules

###Usage
    Usage: python ./wammu2xml.py oldsmsfile.smsbackup xmlsmsfile.xml
